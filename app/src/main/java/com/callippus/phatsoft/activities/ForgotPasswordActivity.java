package com.callippus.phatsoft.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.databinding.ActivityForgotPasswordBinding;

public class ForgotPasswordActivity extends AppCompatActivity
{
   private ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityForgotPasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

}