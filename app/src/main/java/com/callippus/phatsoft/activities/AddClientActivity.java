package com.callippus.phatsoft.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.databinding.ActivityAddClientBinding;

public class AddClientActivity extends AppCompatActivity {

    private ActivityAddClientBinding binding;
    public static final String TAG = "AddClientActivity :";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddClientBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String[] arraySpinnertitle = new String[]{
                "M/s", "Miss", "Mr", "Mrs"
        };
        ArrayAdapter<String> title_adapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item, arraySpinnertitle);
        binding.spinTitle.setAdapter(title_adapter);
        binding.spinTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int sid = binding.spinTitle.getSelectedItemPosition();
                String selected_title = binding.spinTitle.getSelectedItem().toString();
                Log.d(TAG, "Selected title index and name" + sid + selected_title);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        String[] arraySpinner_paymnt_terms = new String[]{
                "30", "60", "90"
        };
        ArrayAdapter<String> paymnt_adapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item, arraySpinner_paymnt_terms);
        binding.paymentTerms.setAdapter(paymnt_adapter);
        binding.paymentTerms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int sid = binding.paymentTerms.getSelectedItemPosition();
                String selected_payment_days = binding.paymentTerms.getSelectedItem().toString();
                Log.d(TAG, "Selected paymnet term days index and name" + sid + selected_payment_days);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}