package com.callippus.phatsoft.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.databinding.ActivityManagementBinding;
import com.callippus.phatsoft.databinding.PersonsDialogBinding;
import com.callippus.phatsoft.databinding.ViewJourneyFilterDialogBinding;
import com.callippus.phatsoft.utills.CheckInternet;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;
import java.util.List;

public class ManagementActivity extends AppCompatActivity {

    private ActivityManagementBinding binding;
    private Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityManagementBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_MinWidth);
        binding.personsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              showPersonsDialog();
            }
        });

        binding.backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        binding.textViewAddsupplier.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent gotoNextactivity=new Intent(ManagementActivity.this,AddSupplierActivity.class);
//                startActivity(gotoNextactivity);
//                finish();
//            }
//        });
//
//        binding.textViewAddclient.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent gotoNextactivity=new Intent(ManagementActivity.this,AddClientActivity.class);
//                startActivity(gotoNextactivity);
//                finish();
//            }
//        });
//
//        binding.textViewEditclient.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent gotoNextactivity=new Intent(ManagementActivity.this,EditClientActivity.class);
//                startActivity(gotoNextactivity);
//                finish();
//            }
//        });

    }

    private void showPersonsDialog()
    {
        final PersonsDialogBinding personsDialogBinding;
        personsDialogBinding = PersonsDialogBinding.inflate(getLayoutInflater());
        dialog.setContentView(personsDialogBinding.getRoot());

        personsDialogBinding.addAccountant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,AddAccountantActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.addClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,AddClientActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.addContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,AddContactActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.addDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,AddDriverActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.addSupplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,AddSupplierActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.editAccountant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,EditAccountantActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.editClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,EditClientActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.editContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,EditContactActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.editDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,EditDriverActivity.class);
                startActivity(intent);
            }
        });
        personsDialogBinding.editSupplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ManagementActivity.this,EditSupplierActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }



}

