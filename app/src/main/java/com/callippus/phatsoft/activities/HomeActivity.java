package com.callippus.phatsoft.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.databinding.ActivityHomeBinding;
import com.callippus.phatsoft.fragments.AddJourneyFragment;
import com.callippus.phatsoft.fragments.PayBillsFragment;
import com.callippus.phatsoft.fragments.ReceivePaymentsFragment;
import com.callippus.phatsoft.fragments.ViewExpensesFragment;
import com.callippus.phatsoft.fragments.ViewJourneysFragment;
import com.callippus.phatsoft.fragments.ViewPaymentsFragment;
import com.google.android.material.navigation.NavigationView;

public class HomeActivity extends AppCompatActivity {
    private ActivityHomeBinding binding;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        toggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolBar, R.string.open, R.string.close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        toggle.syncState();

        addFragment(new AddJourneyFragment());
        binding.navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_add_journey:
                        Fragment f1 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                        if (f1 instanceof AddJourneyFragment) {
                            Toast.makeText(HomeActivity.this, "Already opened", Toast.LENGTH_SHORT).show();
                        } else {
                            replaceFragment(new AddJourneyFragment());
                        }
                        break;

                    case R.id.menu_view_journey:
                        Fragment f2 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                        if (f2 instanceof ViewJourneysFragment) {
                            Toast.makeText(HomeActivity.this, "Already opened", Toast.LENGTH_SHORT).show();
                        } else {
                            replaceFragment(new ViewJourneysFragment());
                        }
                        break;

                    case R.id.menu_view_pymnts:
                        Fragment f3 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                        if (f3 instanceof ViewPaymentsFragment) {
                            Toast.makeText(HomeActivity.this, "Already opened", Toast.LENGTH_SHORT).show();
                        } else {
                            replaceFragment(new ViewPaymentsFragment());
                        }
                        break;
                    case R.id.menu_receive_pymnts:
                        Fragment f4 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                        if (f4 instanceof ReceivePaymentsFragment) {
                            Toast.makeText(HomeActivity.this, "Already opened", Toast.LENGTH_SHORT).show();
                        } else {
                            replaceFragment(new ReceivePaymentsFragment());
                        }
                        break;
                    case R.id.menu_pay_bills:
                        Fragment f5 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                        if (f5 instanceof PayBillsFragment) {
                            Toast.makeText(HomeActivity.this, "Already opened", Toast.LENGTH_SHORT).show();
                        } else {
                            replaceFragment(new PayBillsFragment());
                        }
                        break;
                    case R.id.menu_view_expenses:
                        Fragment f6 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                        if (f6 instanceof ViewExpensesFragment) {
                            Toast.makeText(HomeActivity.this, "Already opened", Toast.LENGTH_SHORT).show();
                        } else {
                            replaceFragment(new ViewExpensesFragment());
                        }
                        break;
                    case R.id.menu_mgmt:
                        Intent gotoNextActivity = new Intent(HomeActivity.this, ManagementActivity.class);
                        startActivity(gotoNextActivity);
                        break;
                    case R.id.menu_logout:
                        Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                        break;

                }
                binding.drawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });

    }


    private void addFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            return;
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            return;
        } else {
            Fragment f3 = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
            if (f3 instanceof AddJourneyFragment) {
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                HomeActivity.super.onBackPressed();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        }
    }

    public void setActionBarTitle(String title) {
        binding.pageTitle.setText(title);
    }

}