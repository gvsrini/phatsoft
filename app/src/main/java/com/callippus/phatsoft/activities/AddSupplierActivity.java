package com.callippus.phatsoft.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.databinding.ActivityAddSupplierBinding;
import com.callippus.phatsoft.databinding.ActivityManagementBinding;

public class AddSupplierActivity extends AppCompatActivity {

    ActivityAddSupplierBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= ActivityAddSupplierBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.idAddSupplierImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoNextactivity=new Intent(AddSupplierActivity.this,ManagementActivity.class);
                startActivity(gotoNextactivity);
                finish();
            }
        });
    }
}