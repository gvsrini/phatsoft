package com.callippus.phatsoft.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.activities.HomeActivity;
import com.callippus.phatsoft.databinding.FragmentPayBillsBinding;
import com.callippus.phatsoft.databinding.ReceivePaymentsBottomSheetBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;


public class PayBillsFragment extends Fragment
{
   private FragmentPayBillsBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding=FragmentPayBillsBinding.inflate(getLayoutInflater(),container,false);
        binding.viewTotalPayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheet();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).setActionBarTitle("PayBills");
    }

    private void showBottomSheet()
    {
        BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(getContext(),R.style.BottomSheetDialogTheme);
        ReceivePaymentsBottomSheetBinding receivePaymentsBottomSheet=ReceivePaymentsBottomSheetBinding.inflate(getLayoutInflater());
        View bottomView=receivePaymentsBottomSheet.getRoot();
        bottomSheetDialog.setContentView(bottomView);
        bottomSheetDialog.show();
    }
}