package com.callippus.phatsoft.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.callippus.phatsoft.activities.HomeActivity;
import com.callippus.phatsoft.databinding.FragmentAddJourneyBinding;

import java.util.Calendar;

public class AddJourneyFragment extends Fragment {

    private FragmentAddJourneyBinding binding;
    private View view;
    DatePickerDialog datepicker;
    TimePickerDialog timePickerDialog;
    CheckBox checkbox_time;
    Context context;
    ImageView img_back, img_add_client, img_add_driver, img_edit_passenger;
    Spinner spn_client, spn_driver, spn_passengers, spn_from, spn_to;
    public static final String TAG = "AddJourneyFragment :";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentAddJourneyBinding.inflate(getLayoutInflater(), container, false);
        view = binding.getRoot();


        Log.d(TAG, "In oncreate Addjourneys");

        binding.etJournetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                datepicker = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                binding.etJournetDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                datepicker.show();
            }


        });



//        /* this is for dropdowns select clients starts here*/

        String[] arraySpinnerclients = new String[]{
                "John", "Mathew", "Jacob", "Peterson"
        };


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.
                R.layout.simple_spinner_dropdown_item, arraySpinnerclients);

        binding.idSpnClient.setAdapter(adapter);

        binding.idSpnClient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int sid = binding.idSpnClient.getSelectedItemPosition();
                String selected_clients = binding.idSpnClient.getSelectedItem().toString();
                Log.d(TAG, "Selected client index and name" + sid + selected_clients);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

//        /* here dropdown end for client*/


        /* this is for dropdowns select driver starts here*/

        String[] arraySpinnerdrivers = new String[]{
                "D1", "D2", "D3"
        };


//        ArrayAdapter<String> adapter_driver = new ArrayAdapter<String>(getActivity(), android.
//                R.layout.simple_spinner_dropdown_item, arraySpinnerdrivers);
//
//        binding.idSpnDriver.setAdapter(adapter_driver);
//
//        binding.idSpnDriver.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                int sid_driver = binding.idSpnDriver.getSelectedItemPosition();
//                String selected_driver = binding.idSpnDriver.getSelectedItem().toString();
//                Log.d(TAG, "Selected driver index and name" + sid_driver + selected_driver);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//            }
//        });
//
////        /* here dropdown end for driver*/
//
//        /* this is for dropdowns select passengers starts here*/
//
//        String[] arraySpinnerpassenger = new String[]{
//                "P1", "P2", "P3"
//        };
//
//
//        ArrayAdapter<String> passenger_adapter = new ArrayAdapter<String>(getActivity(), android.
//                R.layout.simple_spinner_dropdown_item, arraySpinnerpassenger);
//
//        binding.idSpnPassenger.setAdapter(passenger_adapter);
//
//        binding.idSpnPassenger.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                int sid = binding.idSpnPassenger.getSelectedItemPosition();
//                String selected_passenger = binding.idSpnPassenger.getSelectedItem().toString();
//                Log.d(TAG, "Selected passenger index and name" + sid + selected_passenger);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//            }
//        });
//
////        /* here dropdown end for passenger*/
//
//        /* this is for dropdowns select from loc starts here*/
//
//        String[] arraySpinner_fromLoc = new String[]{
//                "Kukatpally", "Abids", "Khairathabad"
//        };
//
//
//        ArrayAdapter<String> fromLoc_adapter = new ArrayAdapter<String>(getActivity(), android.
//                R.layout.simple_spinner_dropdown_item, arraySpinner_fromLoc);
//
//        binding.idSpnFromLoc.setAdapter(fromLoc_adapter);
//
//        binding.idSpnFromLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                int sid = binding.idSpnFromLoc.getSelectedItemPosition();
//                String selected_fromLoc = binding.idSpnFromLoc.getSelectedItem().toString();
//                Log.d(TAG, "Selected from loc index and name" + sid + selected_fromLoc);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//            }
//        });
//
//        /* here dropdown end for from loc*/
//
//        /* this is for dropdowns select to loc starts here*/
//
//        String[] arraySpinner_toLoc = new String[]{
//                "Moosapet", "KPHB", "Miyapur"
//        };
//
//        ArrayAdapter<String> toLoc_adapter = new ArrayAdapter<String>(getActivity(), android.
//                R.layout.simple_spinner_dropdown_item, arraySpinner_toLoc);
//
//        binding.idSpnToLoc.setAdapter(toLoc_adapter);
//
//        binding.idSpnToLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                int sid = binding.idSpnToLoc.getSelectedItemPosition();
//                String selected_toLoc = binding.idSpnToLoc.getSelectedItem().toString();
//                Log.d(TAG, "Selected loc to index and name" + sid + selected_toLoc);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//            }
//        });
//
//        /* here dropdown end for to loc*/
//
//        binding.idImgAddClient.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                getActivity().startActivity(new Intent(getActivity(),AddClientActivity.class));
//                getActivity().finish();
//            }
//        });
////        img_add_driver.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent goToNextActivity = new Intent(
//                        getApplicationContext(), AddDriverActivity.class);
//                startActivity(goToNextActivity);
//                finish();
//            }
//        });
//        img_edit_passenger.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent goToNextActivity = new Intent(
//                        getApplicationContext(), EditPassengerActivity.class);
//                startActivity(goToNextActivity);
//                finish();
//            }
//        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).setActionBarTitle("AddJourney");
    }

    public void selectTime() {
//        binding.cbTime.setInputType(InputType.TYPE_NULL);
//        binding.cbTime.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                final Calendar cldr = Calendar.getInstance();
//                int hour = cldr.get(Calendar.HOUR_OF_DAY);
//                int minutes = cldr.get(Calendar.MINUTE);
//
//                // time picker dialog
//                timePickerDialog = new TimePickerDialog(getActivity(),
//                        new TimePickerDialog.OnTimeSetListener() {
//
//                            @Override
//                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
//
//                                binding.cbTime.setText(sHour + ":" + sMinute + " ");
//                            }
//                        }, hour, minutes, true);
//                timePickerDialog.show();
//            }
//        });
    }
}