package com.callippus.phatsoft.fragments;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.databinding.BottomSheetViewPaymentsBinding;
import com.callippus.phatsoft.databinding.FragmentViewExpensesBinding;
import com.callippus.phatsoft.databinding.ViewExpensesFilterDialogBinding;
import com.callippus.phatsoft.databinding.ViewJourneyFilterDialogBinding;
import com.callippus.phatsoft.utills.CheckInternet;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;
import java.util.List;


public class ViewExpensesFragment extends Fragment
{
    private FragmentViewExpensesBinding binding;
    private Dialog dialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding=FragmentViewExpensesBinding.inflate(getLayoutInflater(),container,false);
        dialog = new Dialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog_MinWidth);
        binding.filterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterDialog();
            }
        });

        binding.viewTotalPayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheet();
            }
        });
        return binding.getRoot();
    }

    private void showBottomSheet()
    {
        BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(getContext(),R.style.BottomSheetDialogTheme);
        BottomSheetViewPaymentsBinding bottomSheetBinding=BottomSheetViewPaymentsBinding.inflate(getLayoutInflater());
        View bottomView=bottomSheetBinding.getRoot();
        bottomSheetDialog.setContentView(bottomView);
        bottomSheetBinding.total.setText("1500 rs");
        bottomSheetDialog.show();
    }

    private void showFilterDialog() {
        final ViewExpensesFilterDialogBinding filterDialog;
        filterDialog = ViewExpensesFilterDialogBinding.inflate(getLayoutInflater());
        dialog.setContentView(filterDialog.getRoot());


        filterDialog.filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckInternet.checkInternet(getContext()) && !CheckInternet.checkVPN(getContext())) {
                    dialog.dismiss();
                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Check Internet", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        filterDialog.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}