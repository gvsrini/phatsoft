package com.callippus.phatsoft.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.activities.HomeActivity;
import com.callippus.phatsoft.databinding.FragmentViewJourneysBinding;
import com.callippus.phatsoft.databinding.ViewJourneyFilterDialogBinding;
import com.callippus.phatsoft.utills.CheckInternet;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class ViewJourneysFragment extends Fragment {
    private FragmentViewJourneysBinding binding;
    private String finalDate = "";
    private Long lastSelectedDate = null;
    private Dialog dialog;
    private String[] selectUsers = {"All", "UserOne","UserTwo"};
    private String[] paymentCategories = {"Paid", "UnPaid"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentViewJourneysBinding.inflate(getLayoutInflater(), container, false);
        dialog = new Dialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog_MinWidth);
        binding.fromSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker(getContext(),"from");
            }
        });
        binding.toSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker(getContext(),"to");
            }
        });
        binding.filterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterDialog();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).setActionBarTitle("ViewJourneys");
    }

    public String datePicker(final Context context, final String tag) {
        final MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker()
                .setSelection(lastSelectedDate == null ? System.currentTimeMillis() : lastSelectedDate);
        builder.setTitleText("Select Date");

        final MaterialDatePicker materialDatePicker = builder.build();
        materialDatePicker.show(((AppCompatActivity) context).getSupportFragmentManager(), "DATE_PICKER");
        materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                lastSelectedDate = (Long) selection;
                Date date = new Date((Long) selection);
                String pattern = "dd-MMM-yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                finalDate = simpleDateFormat.format(date);
//                todaysDate.setValue(finalDate);
//                mutableLiveData = districtRepository.getDistrictData(Integer.parseInt(company_id), finalDate);
                if (tag.equalsIgnoreCase("from"))
                {
                    binding.fromSpinner.setText(finalDate);
                }
                else{
                    binding.toSpinner.setText(finalDate);
                }

            }
        });
        return finalDate;
    }

    public String getSystemTime() {
        Date date = new Date(System.currentTimeMillis());
        String pattern = "dd-MMM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        finalDate = simpleDateFormat.format(date);
//        todaysDate.setValue(finalDate);
        return finalDate;
    }

    private void showFilterDialog() {
        final ViewJourneyFilterDialogBinding filterDialogBinding;
        filterDialogBinding = ViewJourneyFilterDialogBinding.inflate(getLayoutInflater());
        dialog.setContentView(filterDialogBinding.getRoot());
        List<String> usersList = Arrays.asList(selectUsers);
        List<String> paymenmtCategories = Arrays.asList(paymentCategories);
        ArrayAdapter userSpinAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, usersList);
        filterDialogBinding.selectUserSpin.setAdapter(userSpinAdapter);
        ArrayAdapter pamentsSpinAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, paymenmtCategories);
        filterDialogBinding.selectPamentsSpin.setAdapter(pamentsSpinAdapter);

        filterDialogBinding.filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckInternet.checkInternet(getContext()) && !CheckInternet.checkVPN(getContext())) {
                    dialog.dismiss();
                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Check Internet", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        filterDialogBinding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}