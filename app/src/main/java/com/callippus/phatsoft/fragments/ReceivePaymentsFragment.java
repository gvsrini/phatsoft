package com.callippus.phatsoft.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.activities.HomeActivity;
import com.callippus.phatsoft.databinding.BottomSheetViewPaymentsBinding;
import com.callippus.phatsoft.databinding.FragmentReceivePaymentsBinding;
import com.callippus.phatsoft.databinding.ReceivePaymentsBottomSheetBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;


public class ReceivePaymentsFragment extends Fragment {
    private FragmentReceivePaymentsBinding binding;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentReceivePaymentsBinding.inflate(getLayoutInflater(), container, false);
        view = binding.getRoot();
        binding.viewTotalPayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheet();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).setActionBarTitle("ReceivePayments");
    }

    private void showBottomSheet()
    {
        BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(getContext(),R.style.BottomSheetDialogTheme);
        ReceivePaymentsBottomSheetBinding receivePaymentsBottomSheet=ReceivePaymentsBottomSheetBinding.inflate(getLayoutInflater());
        View bottomView=receivePaymentsBottomSheet.getRoot();
        bottomSheetDialog.setContentView(bottomView);
        bottomSheetDialog.show();
    }

}