package com.callippus.phatsoft.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.callippus.phatsoft.R;
import com.callippus.phatsoft.activities.HomeActivity;
import com.callippus.phatsoft.databinding.BottomSheetViewPaymentsBinding;
import com.callippus.phatsoft.databinding.FragmentViewPaymentsBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;


public class ViewPaymentsFragment extends Fragment
{
   private FragmentViewPaymentsBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding=FragmentViewPaymentsBinding.inflate(getLayoutInflater(),container,false);
        binding.viewTotalPayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              showBottomSheet();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).setActionBarTitle("ViewPayments");
    }

    private void showBottomSheet()
    {
        BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(getContext(),R.style.BottomSheetDialogTheme);
        BottomSheetViewPaymentsBinding bottomSheetBinding=BottomSheetViewPaymentsBinding.inflate(getLayoutInflater());
        View bottomView=bottomSheetBinding.getRoot();
        bottomSheetDialog.setContentView(bottomView);
        bottomSheetBinding.total.setText("1500 rs");
        bottomSheetDialog.show();
    }

}